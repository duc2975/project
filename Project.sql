USE [swp_covid_manager]
GO
/****** Object:  Table [dbo].[admin_manager]    Script Date: 3/3/2022 8:53:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_manager](
	[admin_id] [int] IDENTITY(1,1) NOT NULL,
	[account_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[admin_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[city]    Script Date: 3/3/2022 8:53:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[city](
	[city_id] [int] IDENTITY(1,1) NOT NULL,
	[city_name] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[city_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[daily_report]    Script Date: 3/3/2022 8:53:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[daily_report](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[patient_id] [int] NULL,
	[disease_detail_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[disease_detail]    Script Date: 3/3/2022 8:53:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[disease_detail](
	[disease_detail_id] [int] IDENTITY(1,1) NOT NULL,
	[body_temperature] [int] NULL,
	[headache] [int] NULL,
	[fever] [int] NULL,
	[shortness_breath] [int] NULL,
	[loss_of_taste] [int] NULL,
	[diarrhea] [int] NULL,
	[skin_rash] [int] NULL,
	[confusion] [int] NULL,
	[cough] [int] NULL,
	[tired] [int] NULL,
	[sore_throat] [int] NULL,
	[vomiting] [int] NULL,
	[anorexia] [int] NULL,
	[serious_situation] [int] NULL,
	[vaccine_status] [int] NULL,
	[patient_status] [nchar](2) NULL,
	[declaration_time] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[disease_detail_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[district]    Script Date: 3/3/2022 8:53:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[district](
	[district_id] [int] IDENTITY(1,1) NOT NULL,
	[district_name] [nvarchar](255) NULL,
	[city_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[district_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[doctor]    Script Date: 3/3/2022 8:53:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[doctor](
	[doctor_id] [int] IDENTITY(1,1) NOT NULL,
	[branch] [nvarchar](255) NULL,
	[role] [nvarchar](255) NULL,
	[account_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[doctor_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[feedback_user]    Script Date: 3/3/2022 8:53:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[feedback_user](
	[feedback_id] [int] IDENTITY(1,1) NOT NULL,
	[content] [text] NULL,
	[vote] [int] NULL,
	[patient_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[feedback_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[interactive_list]    Script Date: 3/3/2022 8:53:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[interactive_list](
	[interactive_id] [int] IDENTITY(1,1) NOT NULL,
	[interactiveName] [nvarchar](255) NULL,
	[relationship] [nvarchar](255) NULL,
	[interactiveStatus] [nvarchar](255) NULL,
	[patient_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[interactive_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[nurse]    Script Date: 3/3/2022 8:53:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nurse](
	[nurse_id] [int] IDENTITY(1,1) NOT NULL,
	[branch] [nvarchar](255) NULL,
	[account_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[nurse_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[patient]    Script Date: 3/3/2022 8:53:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[patient](
	[patient_id] [int] IDENTITY(1,1) NOT NULL,
	[account_id] [int] NULL,
	[doctor_id] [int] NULL,
	[nurse_id] [int] NULL,
	[doctor_name] [nvarchar](50) NULL,
	[nurse_name] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[patient_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[patient_nurse]    Script Date: 3/3/2022 8:53:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[patient_nurse](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[patient_id] [int] NULL,
	[nurse_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[travel_schedule]    Script Date: 3/3/2022 8:53:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[travel_schedule](
	[travel_id] [int] IDENTITY(1,1) NOT NULL,
	[patient_id] [int] NULL,
	[departure] [nvarchar](255) NULL,
	[destination] [nvarchar](255) NULL,
	[time_go] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[travel_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_account]    Script Date: 3/3/2022 8:53:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_account](
	[account_id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](255) NULL,
	[password] [nvarchar](255) NULL,
	[full_name] [nvarchar](255) NULL,
	[phone_number] [nvarchar](255) NULL,
	[gender] [int] NULL,
	[birth_day] [date] NULL,
	[age] [int] NULL,
	[status] [int] NULL,
	[role_id] [int] NULL,
	[email] [nvarchar](255) NULL,
	[address] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[account_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[daily_report] ON 

INSERT [dbo].[daily_report] ([id], [patient_id], [disease_detail_id]) VALUES (2, 1, 1)
INSERT [dbo].[daily_report] ([id], [patient_id], [disease_detail_id]) VALUES (3, 2, 2)
INSERT [dbo].[daily_report] ([id], [patient_id], [disease_detail_id]) VALUES (4, 3, 3)
INSERT [dbo].[daily_report] ([id], [patient_id], [disease_detail_id]) VALUES (5, 4, 4)
INSERT [dbo].[daily_report] ([id], [patient_id], [disease_detail_id]) VALUES (6, 5, 5)
INSERT [dbo].[daily_report] ([id], [patient_id], [disease_detail_id]) VALUES (7, 6, 6)
INSERT [dbo].[daily_report] ([id], [patient_id], [disease_detail_id]) VALUES (8, 7, 7)
INSERT [dbo].[daily_report] ([id], [patient_id], [disease_detail_id]) VALUES (9, 8, 8)
INSERT [dbo].[daily_report] ([id], [patient_id], [disease_detail_id]) VALUES (10, 9, 9)
INSERT [dbo].[daily_report] ([id], [patient_id], [disease_detail_id]) VALUES (11, 10, 10)
INSERT [dbo].[daily_report] ([id], [patient_id], [disease_detail_id]) VALUES (12, 11, 11)
INSERT [dbo].[daily_report] ([id], [patient_id], [disease_detail_id]) VALUES (13, 12, 12)
INSERT [dbo].[daily_report] ([id], [patient_id], [disease_detail_id]) VALUES (14, 13, 13)
INSERT [dbo].[daily_report] ([id], [patient_id], [disease_detail_id]) VALUES (15, 14, 14)
INSERT [dbo].[daily_report] ([id], [patient_id], [disease_detail_id]) VALUES (16, 15, 15)
INSERT [dbo].[daily_report] ([id], [patient_id], [disease_detail_id]) VALUES (17, 16, 16)
INSERT [dbo].[daily_report] ([id], [patient_id], [disease_detail_id]) VALUES (18, 17, 17)
INSERT [dbo].[daily_report] ([id], [patient_id], [disease_detail_id]) VALUES (20, 1, 18)
SET IDENTITY_INSERT [dbo].[daily_report] OFF
GO
SET IDENTITY_INSERT [dbo].[disease_detail] ON 

INSERT [dbo].[disease_detail] ([disease_detail_id], [body_temperature], [headache], [fever], [shortness_breath], [loss_of_taste], [diarrhea], [skin_rash], [confusion], [cough], [tired], [sore_throat], [vomiting], [anorexia], [serious_situation], [vaccine_status], [patient_status], [declaration_time]) VALUES (1, 36, 0, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'F0', CAST(N'2022-02-10' AS Date))
INSERT [dbo].[disease_detail] ([disease_detail_id], [body_temperature], [headache], [fever], [shortness_breath], [loss_of_taste], [diarrhea], [skin_rash], [confusion], [cough], [tired], [sore_throat], [vomiting], [anorexia], [serious_situation], [vaccine_status], [patient_status], [declaration_time]) VALUES (2, 39, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'F0', CAST(N'2022-01-10' AS Date))
INSERT [dbo].[disease_detail] ([disease_detail_id], [body_temperature], [headache], [fever], [shortness_breath], [loss_of_taste], [diarrhea], [skin_rash], [confusion], [cough], [tired], [sore_throat], [vomiting], [anorexia], [serious_situation], [vaccine_status], [patient_status], [declaration_time]) VALUES (3, 38, 0, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'F0', CAST(N'2022-06-10' AS Date))
INSERT [dbo].[disease_detail] ([disease_detail_id], [body_temperature], [headache], [fever], [shortness_breath], [loss_of_taste], [diarrhea], [skin_rash], [confusion], [cough], [tired], [sore_throat], [vomiting], [anorexia], [serious_situation], [vaccine_status], [patient_status], [declaration_time]) VALUES (4, 37, 1, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'F0', CAST(N'2022-02-10' AS Date))
INSERT [dbo].[disease_detail] ([disease_detail_id], [body_temperature], [headache], [fever], [shortness_breath], [loss_of_taste], [diarrhea], [skin_rash], [confusion], [cough], [tired], [sore_throat], [vomiting], [anorexia], [serious_situation], [vaccine_status], [patient_status], [declaration_time]) VALUES (5, 38, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'F0', CAST(N'2022-02-10' AS Date))
INSERT [dbo].[disease_detail] ([disease_detail_id], [body_temperature], [headache], [fever], [shortness_breath], [loss_of_taste], [diarrhea], [skin_rash], [confusion], [cough], [tired], [sore_throat], [vomiting], [anorexia], [serious_situation], [vaccine_status], [patient_status], [declaration_time]) VALUES (6, 36, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'F0', CAST(N'2022-02-10' AS Date))
INSERT [dbo].[disease_detail] ([disease_detail_id], [body_temperature], [headache], [fever], [shortness_breath], [loss_of_taste], [diarrhea], [skin_rash], [confusion], [cough], [tired], [sore_throat], [vomiting], [anorexia], [serious_situation], [vaccine_status], [patient_status], [declaration_time]) VALUES (7, 37, 0, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'F0', CAST(N'2022-02-10' AS Date))
INSERT [dbo].[disease_detail] ([disease_detail_id], [body_temperature], [headache], [fever], [shortness_breath], [loss_of_taste], [diarrhea], [skin_rash], [confusion], [cough], [tired], [sore_throat], [vomiting], [anorexia], [serious_situation], [vaccine_status], [patient_status], [declaration_time]) VALUES (8, 38, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'F0', CAST(N'2022-02-10' AS Date))
INSERT [dbo].[disease_detail] ([disease_detail_id], [body_temperature], [headache], [fever], [shortness_breath], [loss_of_taste], [diarrhea], [skin_rash], [confusion], [cough], [tired], [sore_throat], [vomiting], [anorexia], [serious_situation], [vaccine_status], [patient_status], [declaration_time]) VALUES (9, 39, 0, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'F0', CAST(N'2022-02-10' AS Date))
INSERT [dbo].[disease_detail] ([disease_detail_id], [body_temperature], [headache], [fever], [shortness_breath], [loss_of_taste], [diarrhea], [skin_rash], [confusion], [cough], [tired], [sore_throat], [vomiting], [anorexia], [serious_situation], [vaccine_status], [patient_status], [declaration_time]) VALUES (10, 36, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'F0', CAST(N'2022-02-10' AS Date))
INSERT [dbo].[disease_detail] ([disease_detail_id], [body_temperature], [headache], [fever], [shortness_breath], [loss_of_taste], [diarrhea], [skin_rash], [confusion], [cough], [tired], [sore_throat], [vomiting], [anorexia], [serious_situation], [vaccine_status], [patient_status], [declaration_time]) VALUES (11, 38, 1, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'F0', CAST(N'2022-02-10' AS Date))
INSERT [dbo].[disease_detail] ([disease_detail_id], [body_temperature], [headache], [fever], [shortness_breath], [loss_of_taste], [diarrhea], [skin_rash], [confusion], [cough], [tired], [sore_throat], [vomiting], [anorexia], [serious_situation], [vaccine_status], [patient_status], [declaration_time]) VALUES (12, 38, 1, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'F0', CAST(N'2022-02-10' AS Date))
INSERT [dbo].[disease_detail] ([disease_detail_id], [body_temperature], [headache], [fever], [shortness_breath], [loss_of_taste], [diarrhea], [skin_rash], [confusion], [cough], [tired], [sore_throat], [vomiting], [anorexia], [serious_situation], [vaccine_status], [patient_status], [declaration_time]) VALUES (13, 36, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'F0', CAST(N'2022-05-10' AS Date))
INSERT [dbo].[disease_detail] ([disease_detail_id], [body_temperature], [headache], [fever], [shortness_breath], [loss_of_taste], [diarrhea], [skin_rash], [confusion], [cough], [tired], [sore_throat], [vomiting], [anorexia], [serious_situation], [vaccine_status], [patient_status], [declaration_time]) VALUES (14, 38, 0, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'F0', CAST(N'2022-02-10' AS Date))
INSERT [dbo].[disease_detail] ([disease_detail_id], [body_temperature], [headache], [fever], [shortness_breath], [loss_of_taste], [diarrhea], [skin_rash], [confusion], [cough], [tired], [sore_throat], [vomiting], [anorexia], [serious_situation], [vaccine_status], [patient_status], [declaration_time]) VALUES (15, 36, 0, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'F0', CAST(N'2022-02-10' AS Date))
INSERT [dbo].[disease_detail] ([disease_detail_id], [body_temperature], [headache], [fever], [shortness_breath], [loss_of_taste], [diarrhea], [skin_rash], [confusion], [cough], [tired], [sore_throat], [vomiting], [anorexia], [serious_situation], [vaccine_status], [patient_status], [declaration_time]) VALUES (16, 39, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2022-02-10' AS Date))
INSERT [dbo].[disease_detail] ([disease_detail_id], [body_temperature], [headache], [fever], [shortness_breath], [loss_of_taste], [diarrhea], [skin_rash], [confusion], [cough], [tired], [sore_throat], [vomiting], [anorexia], [serious_situation], [vaccine_status], [patient_status], [declaration_time]) VALUES (17, 38, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2022-03-03' AS Date))
INSERT [dbo].[disease_detail] ([disease_detail_id], [body_temperature], [headache], [fever], [shortness_breath], [loss_of_taste], [diarrhea], [skin_rash], [confusion], [cough], [tired], [sore_throat], [vomiting], [anorexia], [serious_situation], [vaccine_status], [patient_status], [declaration_time]) VALUES (18, 37, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, N'F0', CAST(N'2022-03-03' AS Date))
SET IDENTITY_INSERT [dbo].[disease_detail] OFF
GO
SET IDENTITY_INSERT [dbo].[doctor] ON 

INSERT [dbo].[doctor] ([doctor_id], [branch], [role], [account_id]) VALUES (1, N'lung', N'experienced doctor', 2)
INSERT [dbo].[doctor] ([doctor_id], [branch], [role], [account_id]) VALUES (2, N'lung', N'experienced doctor', 6)
INSERT [dbo].[doctor] ([doctor_id], [branch], [role], [account_id]) VALUES (3, N'lung', N'experienced doctor', 10)
INSERT [dbo].[doctor] ([doctor_id], [branch], [role], [account_id]) VALUES (4, N'lung', N'experienced doctor', 11)
INSERT [dbo].[doctor] ([doctor_id], [branch], [role], [account_id]) VALUES (5, N'lung', N'experienced doctor', 12)
INSERT [dbo].[doctor] ([doctor_id], [branch], [role], [account_id]) VALUES (6, N'lung', N'experienced doctor', 13)
INSERT [dbo].[doctor] ([doctor_id], [branch], [role], [account_id]) VALUES (7, N'lung', N'experienced doctor', 14)
INSERT [dbo].[doctor] ([doctor_id], [branch], [role], [account_id]) VALUES (8, N'lung', N'experienced doctor', 15)
INSERT [dbo].[doctor] ([doctor_id], [branch], [role], [account_id]) VALUES (9, N'lung', N'experienced doctor', 16)
INSERT [dbo].[doctor] ([doctor_id], [branch], [role], [account_id]) VALUES (10, N'lung', N'experienced doctor', 17)
INSERT [dbo].[doctor] ([doctor_id], [branch], [role], [account_id]) VALUES (11, N'lung', N'experienced doctor', 18)
SET IDENTITY_INSERT [dbo].[doctor] OFF
GO
SET IDENTITY_INSERT [dbo].[nurse] ON 

INSERT [dbo].[nurse] ([nurse_id], [branch], [account_id]) VALUES (5, N'lung', 5)
INSERT [dbo].[nurse] ([nurse_id], [branch], [account_id]) VALUES (6, N'lung', 8)
INSERT [dbo].[nurse] ([nurse_id], [branch], [account_id]) VALUES (7, N'lung', 9)
INSERT [dbo].[nurse] ([nurse_id], [branch], [account_id]) VALUES (8, N'Community Health', 20)
INSERT [dbo].[nurse] ([nurse_id], [branch], [account_id]) VALUES (9, N'Community Health', 21)
INSERT [dbo].[nurse] ([nurse_id], [branch], [account_id]) VALUES (10, N'Community Health', 22)
INSERT [dbo].[nurse] ([nurse_id], [branch], [account_id]) VALUES (11, N'Community Health', 23)
INSERT [dbo].[nurse] ([nurse_id], [branch], [account_id]) VALUES (12, N'Community Health', 24)
INSERT [dbo].[nurse] ([nurse_id], [branch], [account_id]) VALUES (13, N'Community Health', 25)
INSERT [dbo].[nurse] ([nurse_id], [branch], [account_id]) VALUES (14, N'Community Health', 26)
INSERT [dbo].[nurse] ([nurse_id], [branch], [account_id]) VALUES (15, N'Community Health', 27)
INSERT [dbo].[nurse] ([nurse_id], [branch], [account_id]) VALUES (16, N'Community Health', 28)
INSERT [dbo].[nurse] ([nurse_id], [branch], [account_id]) VALUES (17, N'Community Health', 29)
SET IDENTITY_INSERT [dbo].[nurse] OFF
GO
SET IDENTITY_INSERT [dbo].[patient] ON 

INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (1, 3, 2, 5, N'Nguyen Ba Binh', N'Vu Bich Ngoc')
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (2, 4, 2, 5, N'Nguyen Ba Binh', N'Vu Bich Ngoc')
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (3, 7, 2, 5, N'Nguyen Ba Binh', N'Vu Bich Ngoc')
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (4, 29, 2, 5, N'Nguyen Ba Binh', N'Vu Bich Ngoc')
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (5, 33, 2, NULL, N'Nguyen Ba Binh', NULL)
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (6, 34, 2, NULL, N'Nguyen Ba Binh', NULL)
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (7, 35, 2, NULL, N'Nguyen Ba Binh', NULL)
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (8, 36, 2, NULL, N'Nguyen Ba Binh', NULL)
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (9, 37, 2, NULL, N'Nguyen Ba Binh', NULL)
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (10, 38, 2, NULL, N'Nguyen Ba Binh', NULL)
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (11, 39, 2, NULL, N'Nguyen Ba Binh', NULL)
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (12, 40, 2, NULL, N'Nguyen Ba Binh', NULL)
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (13, 41, 2, NULL, N'Nguyen Ba Binh', NULL)
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (14, 42, 2, NULL, N'Nguyen Ba Binh', NULL)
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (15, 43, 2, NULL, N'Nguyen Ba Binh', NULL)
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (16, 46, 2, NULL, N'Nguyen Ba Binh', NULL)
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (17, 50, 2, NULL, N'Nguyen Ba Binh', NULL)
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (18, 51, 2, NULL, N'Nguyen Ba Binh', NULL)
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (19, 52, 2, NULL, N'Nguyen Ba Binh', NULL)
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (20, 46, 2, NULL, N'Nguyen Ba Binh', NULL)
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (21, 53, 2, NULL, N'Nguyen Ba Binh', NULL)
INSERT [dbo].[patient] ([patient_id], [account_id], [doctor_id], [nurse_id], [doctor_name], [nurse_name]) VALUES (22, 54, 2, NULL, N'Nguyen Ba Binh', NULL)
SET IDENTITY_INSERT [dbo].[patient] OFF
GO
SET IDENTITY_INSERT [dbo].[user_account] ON 

INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (1, N'tuannam', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Vu Tuan Nam', N'0345988311', 1, NULL, 21, 1, 1, NULL, NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (2, N'hanguyen123', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Nguyen Ha', N'0342988311', 1, NULL, 30, 1, 2, N'hanguyen@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (3, N'phuc123456', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Nguyen Van Phuc', N'0345983311', 1, NULL, 22, 1, 4, N'phucpro@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (4, N'duynb123', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Nguyen Ba Duy', N'0345783311', 1, NULL, 40, 1, 4, N'duynb@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (5, N'ngochh66', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Vu Bich Ngoc', N'0345783321', 0, NULL, 22, 1, 3, N'ngochh@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (6, N'binhba123', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Nguyen Ba Binh', N'0345783322', 1, NULL, 34, 1, 2, N'binhbb@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (7, N'giangtrang86', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Dang Giang', N'0345783326', 1, NULL, 18, 1, 4, N'giangdo@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (8, N'minhbeo567', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Quach Minh', N'0345783328', 1, NULL, 22, 1, 3, N'minhbeo@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (9, N'tampham17', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Pham Thi Tam', N'0345783356', 0, NULL, 23, 1, 3, N'tampham@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (10, N'doctor1', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Doctor 1', N'0345783352', 0, NULL, 29, 1, 2, N'doctor1@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (11, N'doctor2', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Doctor 2', N'0345783366', 0, NULL, 29, 1, 2, N'doctor2@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (12, N'doctor3', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Doctor 3', N'0345783377', 1, NULL, 29, 1, 2, N'doctor3@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (13, N'doctor4', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Doctor 4', N'0345783388', 0, NULL, 34, 1, 2, N'doctor4@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (14, N'doctor5', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Doctor 5', N'0345783312', 1, NULL, 30, 1, 2, N'doctor5@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (15, N'doctor6', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Doctor 6', N'0345783333', 1, NULL, 29, 1, 2, N'doctor6@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (16, N'doctor7', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Doctor 7', N'0345783369', 0, NULL, 29, 1, 2, N'doctor7@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (17, N'doctor8', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Doctor 8', N'0345783123', 0, NULL, 26, 1, 2, N'doctor8@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (18, N'doctor9', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Doctor 9', N'0345783345', 0, NULL, 29, 1, 2, N'doctor9@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (19, N'doctor10', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Doctor 10', N'0345783343', 0, NULL, 29, 1, 2, N'doctor1@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (20, N'nurse1', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Nurse 1', N'0395783343', 0, NULL, 22, 1, 3, N'nurse1@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (21, N'nurse2', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Nurse 2', N'0385783343', 0, NULL, 24, 1, 3, N'nurse2@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (22, N'nurse3', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Nurse 3', N'0345583343', 0, NULL, 21, 1, 3, N'nurse3@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (23, N'nurse4', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Nurse 4', N'0347783343', 0, NULL, 25, 1, 3, N'nurse4@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (24, N'nurse5', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Nurse 5', N'0945783343', 0, NULL, 30, 1, 3, N'nurse5@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (25, N'nurse6', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Nurse 6', N'0915783343', 0, NULL, 24, 1, 3, N'nurse6@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (26, N'nurse7', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Nurse 7', N'0355783343', 0, NULL, 21, 1, 3, N'nurse7@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (27, N'nurse8', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Nurse 8', N'0975783343', 0, NULL, 22, 1, 3, N'nurse8@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (28, N'nurse9', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Nurse 9', N'0345333343', 0, NULL, 23, 1, 3, N'nurse9@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (29, N'patient1', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Patien 1', N'0945783342', 0, NULL, 22, 1, 4, N'patient1@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (33, N'patient2', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Patien 2', N'0945783342', 0, NULL, 24, 1, 4, N'patient2@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (34, N'patient3', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Patien 3', N'0945783342', 0, NULL, 25, 1, 4, N'patient13@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (35, N'patient4', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Patien 4', N'0945783342', 0, NULL, 21, 1, 4, N'patient11@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (36, N'patient5', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Patien 5', N'0945783342', 0, NULL, 22, 1, 4, N'patient221@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (37, N'patient6', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Patien 6', N'0945783342', 1, NULL, 26, 1, 4, N'patient133@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (38, N'patient7', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Patien 7', N'0945783342', 0, NULL, 21, 1, 4, N'patien1t1@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (39, N'patient8', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Patien 8', N'0945783342', 0, NULL, 22, 1, 4, N'patient331@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (40, N'patient9', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Patien 9', N'0945783342', 0, NULL, 17, 1, 4, N'patien33t1@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (41, N'patient10', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Patien 10', N'0945783342', 0, NULL, 22, 1, 4, N'patienttt1@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (42, N'patient11', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Patien 11', N'0945783342', 1, NULL, 22, 1, 4, N'patient1231@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (43, N'patient12', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Patien 12', N'0945783342', 0, NULL, 22, 1, 4, N'patient1231@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (44, N'patient1', N'u3CoY4K5gxXQqWDHFIweUQ==', N'Patien 13', N'0945783342', 1, NULL, 22, 1, 4, N'patient1123@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (45, N'namtien123', N'xkJtc5EOrYz6+tROaFY6KA==', N'VU TUAN NAM', N'0123456789', 1, NULL, 23, 1, 1, NULL, NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (46, N'thiagosilva6120', N'ItsI6hxJD5vtWG4Fxtieuw==', N'Vu Tuan Nam', N'0123456789', 1, NULL, 21, 0, 4, N'namthiagosilva@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (47, N'thiagosilva61200', N'ItsI6hxJD5vtWG4Fxtieuw==', N'Vu Tuan Nam', N'0123456789', 1, NULL, 21, 1, 4, N'namthiagosilva@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (48, N'duynb123', N'ItsI6hxJD5vtWG4Fxtieuw==', N'Vu Tuan Nam', N'0123456789', 1, NULL, 21, 1, 4, N'namthiagosilva@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (49, N'thiagosilva6120', N'ItsI6hxJD5vtWG4Fxtieuw==', N'Vu Tuan Nam', N'0123456789', 1, NULL, 21, 1, 4, N'namthiagosilva@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (50, N'thiagosilva61201111', N'ItsI6hxJD5vtWG4Fxtieuw==', N'Vu Tuan Nam', N'0123456789', 1, NULL, 21, 1, 4, N'namthiagosilva@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (51, N'phuc1234563333', N'ItsI6hxJD5vtWG4Fxtieuw==', N'Vu Tuan Nam', N'0123456789', 1, NULL, 21, 1, 4, N'namthiagosilva2222@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (52, N'phuc1234568888', N'ItsI6hxJD5vtWG4Fxtieuw==', N'Vu Tuan Nam', N'0123456787', 1, NULL, 21, 1, 4, N'namthiagosilvaa@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (53, N'meomeo2000', N'ItsI6hxJD5vtWG4Fxtieuw==', NULL, N'0125546789', 1, NULL, 21, 1, 4, N'namtssaa@gmail.com', NULL)
INSERT [dbo].[user_account] ([account_id], [username], [password], [full_name], [phone_number], [gender], [birth_day], [age], [status], [role_id], [email], [address]) VALUES (54, N'messi6120', N'ItsI6hxJD5vtWG4Fxtieuw==', NULL, N'0123411333', 1, CAST(N'2022-02-10' AS Date), 27, 1, 4, N'nam123@gmail.com', NULL)
SET IDENTITY_INSERT [dbo].[user_account] OFF
GO
ALTER TABLE [dbo].[admin_manager]  WITH CHECK ADD FOREIGN KEY([account_id])
REFERENCES [dbo].[user_account] ([account_id])
GO
ALTER TABLE [dbo].[daily_report]  WITH CHECK ADD FOREIGN KEY([disease_detail_id])
REFERENCES [dbo].[disease_detail] ([disease_detail_id])
GO
ALTER TABLE [dbo].[daily_report]  WITH CHECK ADD FOREIGN KEY([disease_detail_id])
REFERENCES [dbo].[disease_detail] ([disease_detail_id])
GO
ALTER TABLE [dbo].[daily_report]  WITH CHECK ADD FOREIGN KEY([patient_id])
REFERENCES [dbo].[patient] ([patient_id])
GO
ALTER TABLE [dbo].[daily_report]  WITH CHECK ADD FOREIGN KEY([patient_id])
REFERENCES [dbo].[patient] ([patient_id])
GO
ALTER TABLE [dbo].[district]  WITH CHECK ADD  CONSTRAINT [FK_district_city] FOREIGN KEY([city_id])
REFERENCES [dbo].[city] ([city_id])
GO
ALTER TABLE [dbo].[district] CHECK CONSTRAINT [FK_district_city]
GO
ALTER TABLE [dbo].[doctor]  WITH CHECK ADD FOREIGN KEY([account_id])
REFERENCES [dbo].[user_account] ([account_id])
GO
ALTER TABLE [dbo].[feedback_user]  WITH CHECK ADD FOREIGN KEY([patient_id])
REFERENCES [dbo].[patient] ([patient_id])
GO
ALTER TABLE [dbo].[interactive_list]  WITH CHECK ADD FOREIGN KEY([patient_id])
REFERENCES [dbo].[patient] ([patient_id])
GO
ALTER TABLE [dbo].[interactive_list]  WITH CHECK ADD FOREIGN KEY([patient_id])
REFERENCES [dbo].[patient] ([patient_id])
GO
ALTER TABLE [dbo].[nurse]  WITH CHECK ADD FOREIGN KEY([account_id])
REFERENCES [dbo].[user_account] ([account_id])
GO
ALTER TABLE [dbo].[patient]  WITH CHECK ADD FOREIGN KEY([account_id])
REFERENCES [dbo].[user_account] ([account_id])
GO
ALTER TABLE [dbo].[patient]  WITH CHECK ADD FOREIGN KEY([doctor_id])
REFERENCES [dbo].[doctor] ([doctor_id])
GO
ALTER TABLE [dbo].[patient]  WITH CHECK ADD FOREIGN KEY([doctor_id])
REFERENCES [dbo].[doctor] ([doctor_id])
GO
ALTER TABLE [dbo].[patient_nurse]  WITH CHECK ADD FOREIGN KEY([nurse_id])
REFERENCES [dbo].[nurse] ([nurse_id])
GO
ALTER TABLE [dbo].[patient_nurse]  WITH CHECK ADD FOREIGN KEY([nurse_id])
REFERENCES [dbo].[nurse] ([nurse_id])
GO
ALTER TABLE [dbo].[patient_nurse]  WITH CHECK ADD FOREIGN KEY([patient_id])
REFERENCES [dbo].[patient] ([patient_id])
GO
ALTER TABLE [dbo].[patient_nurse]  WITH CHECK ADD FOREIGN KEY([patient_id])
REFERENCES [dbo].[patient] ([patient_id])
GO
ALTER TABLE [dbo].[travel_schedule]  WITH CHECK ADD FOREIGN KEY([patient_id])
REFERENCES [dbo].[patient] ([patient_id])
GO
ALTER TABLE [dbo].[travel_schedule]  WITH CHECK ADD FOREIGN KEY([patient_id])
REFERENCES [dbo].[patient] ([patient_id])
GO
